﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


enum SpawnState
{
    Initial,
    Post
}


public class RandomItemGenerator : MonoBehaviour
{
    [SerializeField]
    public double[] spawn_probabiity;
    int num_items;
    MeshRenderer renderer;
    BoxCollider collider;
    SpawnState spawn_state = SpawnState.Initial;

    // The prefab items to be generated
    [SerializeField]
    GameObject[] items;

    // Start is called before the first frame update
    private void Awake()
    {
        renderer = GetComponent<MeshRenderer>();
        collider = GetComponent<BoxCollider>();
        num_items = items.Length;
    }
    void Start()
    {
        Debug.Log("Hi");
        collider.enabled = true;
        renderer.enabled = true;
        collider.isTrigger = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (spawn_state == SpawnState.Initial && other.tag == "Explosion")
        {
            Random_Spawn();
            spawn_state = SpawnState.Post;
            collider.enabled = false;
            renderer.enabled = false;
        }
    }

    // Spawn an random item according to probability
    void Random_Spawn()
    {
        System.Random rnd = new System.Random();
        double prob = rnd.NextDouble();
        Debug.Log(prob);
        double pre = 0.0d;
        for (int i = 0; i < num_items; i++)
        {
            Debug.Log("pre:"+pre);
            double next = pre+spawn_probabiity[i];
            Debug.Log("next:"+next);
            if (pre <= prob && prob < next)
            {
                Instantiate(items[i], transform.position, new Quaternion(0, 0, 0, 0));
            }
            pre = next;
        }
    }
}
