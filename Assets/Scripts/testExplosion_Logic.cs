﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testExplosion_Logic : MonoBehaviour
{
    float m_horizontalInput;
    float m_verticalInput;

    float m_movementSpeed = 5.0f;

    float m_gravity = 0.981f;
    float m_jumpHeight = 0.25f;

    Vector3 m_horizontalMovement;
    Vector3 m_verticalMovement;
    Vector3 m_heightMovement;

    bool m_jump = false;

    CharacterController m_characterController;


    // Start is called before the first frame update
    void Start()
    {
        m_characterController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        m_horizontalInput = Input.GetAxis("Horizontal");
        m_verticalInput = Input.GetAxis("Vertical");

        if (Input.GetButtonDown("Jump") && m_characterController.isGrounded)
        {
            m_jump = true;
        }
    }

    private void FixedUpdate()
    {
        if (m_jump)
        {
            m_heightMovement.y = m_jumpHeight;
            m_jump = false;
        }

        m_heightMovement.y -= m_gravity * Time.deltaTime;

        m_horizontalMovement = transform.right * m_horizontalInput * m_movementSpeed * Time.deltaTime;
        m_verticalMovement = transform.forward * m_verticalInput * m_movementSpeed * Time.deltaTime;

        // Apply movement
        m_characterController.Move(m_horizontalMovement + m_heightMovement+m_verticalMovement);

        if (m_characterController.isGrounded)
        {
            m_heightMovement.y = 0.0f;
        }
    }
}
